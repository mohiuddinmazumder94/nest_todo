import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  private users: any = [
    { id: 0, name: 'lorem' },
    { id: 1, name: 'ali' },
    { id: 2, name: 'markas' },
    { id: 4, name: 'hello' },
    { id: 5, name: 'lukas' },
  ];

  create(createUserDto: CreateUserDto) {
    const newuser = {
      ...createUserDto,
      id: Math.random() * 10000,
    };
    this.users.push(newuser);
    return newuser;
  }

  findAll(name?: string) {
    if (name) {
      return this.users.filter((user) => user.name === name);
    }
    return this.users;
  }

  findOne(id: number) {
    return this.users.find((user) => user.id === id);
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    this.users = this.users.map((user) => {
      if (user.id === id) {
        return { ...user, ...updateUserDto };
      }
      return user;
    });
    return this.findOne(id);
  }

  remove(id: number) {
    return this.users.filter((user) => user.id !== id);
  }
}
