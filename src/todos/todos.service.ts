import { Injectable } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';

@Injectable()
export class TodosService {
  private todos = [
    {
      id: 0,
      title: '1',
      desc: 'lorem',
    },
    {
      id: 1,
      title: '1sf',
      desc: 'lorem',
    },
  ];
  create(createTodoDto: CreateTodoDto) {
    const newTodo = {
      ...createTodoDto,
      id: Math.random() * 1000,
    };
    this.todos.push(newTodo);
    return newTodo;
  }

  findAll() {
    return this.todos;
  }

  findOne(id: number) {
    return this.todos.filter((item) => item.id === id);
  }

  update(id: number, updateTodoDto: UpdateTodoDto) {
    this.todos = this.todos.map((item) => {
      if (item.id === id) {
        return { ...item, ...updateTodoDto };
      }
      return item;
    });
    return this.findOne(id);
  }

  remove(id: number) {
    return this.todos.filter((item) => item.id !== id);
  }
}
