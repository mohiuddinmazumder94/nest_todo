export class CreateTodoDto {
  title: string;
  desc: string;
}
