import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const config: PostgresConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  password: 'root',
  username: 'shahin',
  entities: [],
  database: 'pgWithNest',
  synchronize: true,
  logging: true,
};

export default config;
